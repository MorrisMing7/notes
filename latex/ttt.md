黎曼猜想
$$
\sum_n{\frac{1}{n^s}}=\prod_p{(1-p^{-s})^{-1}}
$$
傅里叶变换
$$
F(\omega)=\mathcal{F}(f(t))=\int_{-\infty}^{+\infty}{f(t)e^{i\omega t}dt}
$$

$$
\hat{f}(\kappa)=\int_{\mathbb{T}^n}{f(x)e^{-2\pi i\kappa x}dx}
$$



希腊
$$
\begin{matrix} 
  \alpha\Alpha & \beta\Beta & \gamma\Gamma & \delta\Delta \\
  \epsilon\Epsilon\varepsilon & \zeta\Zeta & \eta\Eta & \theta\Theta

\end{matrix}
$$


![image-20230213134733029](C:\myGitlab\notes\latex\alpha_α.png)

![image-20230213135031092](C:\myGitlab\notes\latex\beta_β.png)

