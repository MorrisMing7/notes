*nginx.conf*
```shell
# worker_processes配置项表示开启几个业务进程，一般和cpu核数有关
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    # include表示可以引入其他文件，此处表示引入http mime类型
    client_max_body_size 4096m;
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;

    # 虚拟主机，可以配置多个
    server {
        listen       80;
        server_name  localhost;
        location / {
            alias '/data/';
        }

        location /api/ {
            root   html;
            index  index.html index.htm;
            proxy_pass  http://gateway:8319/;
            proxy_set_header       Host $host;
            proxy_set_header  X-Real-IP  $remote_addr;
            proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }

        location /relationApi/ {
            proxy_pass  http://10.10.10.156:8096/;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
```
- 启动
```shell
docker run -itd -p 2022:80 --name nginx --restart=always -v $(pwd)/webStaticData:/data -v $(pwd)/nginx.conf:/etc/nginx/nginx.conf nginx:1.22.0
```