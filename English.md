- This is a **deliberate** decision in Rust’s design: requiring type annotations in function definitions means the compiler 
almost never needs you to use them elsewhere in the code to figure out what type you mean. 
  > adj
  > - done on purpose rather than by accident    
  > - done slowly and carefully
  > 
  > verb    
  > - to think very carefully about sth, usually before making a decision

- **prevalent**
  > that exists or is very common at a particular time or in a particular place

- the compiler would be more complex and would make fewer guarantees about the code 
if it had to keep track of multiple **hypothetical** types for any variable.
  > adj  
  > If something is hypothetical, it is based on possible ideas or situations rather than actual ones.

Loop Labels to **Disambiguate** Between Multiple Loops
  > to show clearly the difference between two or more words, phrases, etc. which are similar in meaning

As a more **concise** alternative, you can use a for loop and execute some code for each item in a collection.
  > Something that is concise says everything that is necessary without using any unnecessary words.
  > A concise edition of a book, especially a dictionary, is shorter than the original edition.

The type that **signifies** “string slice” is written as &str
  >If an event, a sign, or a symbol signifies something, it is a sign of that thing or represents that thing.
  >
  >If you signify something, you make a sign or gesture in order to communicate a particular meaning.
















