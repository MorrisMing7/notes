docker exec -it a9928871a205 /bin/bash
rsync -avz /var/lib/docker/ /home/varLibDocker/

vi /usr/lib/systemd/system/docker.service
在此行尾添加
    ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock        --graph=/home/varLibDocker/
systemctl daemon-reload && systemctl restart docker



临时修改
sysctl -w vm.max_map_count=262144
永久修改
vi  /etc/sysctl.conf
vm.max_map_count=262144


## docker service 迟迟无法启动
```shell
[root@host-192-173-32-78 ~]# docker service ps gserver_registry
ID                  NAME                 IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR                              PORTS
kizvnjz8x1wl        gserver_registry.1   registry:2.7                            Running             Pending 50 seconds ago   "no suitable node (1 node not …"
[root@host-192-173-32-78 ~]# docker inspect kizvnjz8x1wl
[
    {
        "ID": "kizvnjz8x1wl07gd7cwxiyfzb",
        "Version": {
            "Index": 4898135
        },
        "CreatedAt": "2023-03-16T08:37:18.99956072Z",
        "UpdatedAt": "2023-03-16T08:37:19.053528451Z",
        "Labels": {},
        "Spec": {
            "ContainerSpec": {
                "Image": "registry:2.7",
                "Mounts": [
                    {
                        "Type": "bind",
                        "Source": "/home/sky/gserver/platform-data/registryData",
                        "Target": "/var/lib/registry"
                    }
                ],
                "Isolation": "default"
            },
            "Resources": {
                "Limits": {
                    "MemoryBytes": 268435456
                }
            },
            "Placement": {
                "Constraints": [
                    "node.role==manager"
                ]
            },
            "Networks": [
                {
                    "Target": "kxwhpxbkxzh9h4jtal3e05wsl"
                }
            ],
            "ForceUpdate": 0
        },
        "ServiceID": "sjqjetsqlqij4rum8ym0xuq82",
        "Slot": 1,
        "Status": {
            "Timestamp": "2023-03-16T08:37:19.053461578Z",
            "State": "pending",
            "Message": "pending task scheduling",
            "Err": "no suitable node (1 node not available for new tasks)",
            "PortStatus": {}
        },
。。。。
```
- 解决方法 https://github.com/caprover/caprover/issues/619

```shell
sudo systemctl stop docker
sudo rm /var/lib/docker/swarm/worker/tasks.db
sudo systemctl start docker
```
