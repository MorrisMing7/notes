https://blog.csdn.net/zhiwenganyong/article/details/122515839
>https://download.dameng.com/eco/dm8/dm8_docker_case.tar    
>docker run -itd -p 5236:5236 --name dm8_01 dm8:v01 /bin/bash /startDm.sh


```sql
create tablespace "gserver" datafile '/data/gserver.dbf' size 1024 autoextend on next 512 CACHE = NORMAL;
sp_set_para_value(1,'PWD_POLICY',0);   -- 解除密码长度限制
create user postgres identified by postgres default tablespace "test_table_space" ;

```
#### 用户 模式
```sql
create user "GV_MAPDATA" identified by "GV_MAPDATA"  
default tablespace "gserver";

grant "DBA","PUBLIC","SOI","VTI" to "GV_MAPDATA";

CREATE SCHEMA "GV_MAPDATA" AUTHORIZATION "GV_MAPDATA";
```