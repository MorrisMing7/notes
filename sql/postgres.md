### 日志
*/var/lib/postgresql/data/postgresql.conf*
```config
logging_collector = on
log_directory = 'log'
log_filename = 'postgresql-%m-%d.log'
log_truncate_on_rotation = on
log_rotation_size = 10MB
log_statement = 'ddl'
log_lock_waits = on
log_min_duration_statement = 1500
log_connections = on
log_disconnections = on
log_checkpoints = on
log_line_prefix = '%e: %t [%p]: [%l-1] user = %u,db = %d,remote = %r app = %a'
```

sql
```sql
--- 重载配置 
SELECT pg_reload_conf();
--- 查看log相关配置 
select name,setting,short_desc from pg_settings where name like 'log_%';
--- 查看日志目录
SHOW log_directory ;
--- 查看日志文件
select * from pg_ls_logdir() order by modification; 
```