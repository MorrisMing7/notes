### layer
GeM 广义均值
### 距离
L2 cos
### 损失函数
L-Softmax loss，强化特征的类内紧凑和类间疏离

##pytorch
### 序
- pytorch 动态图 tensorflow 静态图
- pytorch 版本兼容性更好
- ONNX是微软和Facebook提出用来表示深度学习模型的开放格式
### 功能
1. gpu 加速
    ```python
    import torch
    
    a = torch.randn(20000,3000)
    b = torch.randn(3000,40000)
    
    device = torch.device('cuda')
    
    ac = a.to(device)
    bc = b.to(device)
    
    import time 
    t1 = time.time()
    x1 = torch.matmul(a,b)
    print(time.time()-t1)
    
    t1 = time.time()
    x2 = torch.matmul(ac,bc)
    print(time.time()-t1)
    ```
2. 自动求导
   ```python
   import torch
   torch.set_default_dtype(torch.float32)
   a = torch.randn(20000,3000,requires_grad=True)
   b = torch.randn(3000,40000,requires_grad=True)
   device = torch.device('cuda')
   ac = a.to(device)
   bc = b.to(device)
   y = torch.norm(torch.matmul(ac,bc))
   grad = torch.autograd.grad(y,[ac,bc])
   print(grad)
   ```
3. 常见网络层
   
### 线性回归

### 卷积网络
- 卷积
   ```python
   import torch
   torch.set_default_dtype(torch.float32)
   x = torch.rand(1,3,28,28)
   layer1 = torch.nn.Conv2d(in_channels=3,out_channels=6,kernel_size=3,stride=1,padding=0)
   x1 = layer1(x)
   layer2 = torch.nn.Conv2d(in_channels=6,out_channels=12,kernel_size=3,stride=1,padding=1)
   x2 = layer2(x1)
   print(x2.size())
   layer3 = torch.nn.Conv2d(in_channels=12,out_channels=24,kernel_size=3,stride=2,padding=1)
   x3=layer3(x2)
   print(x3.size())
   ```
- image normalize
   ```python
   from torchvision import transforms
   transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225] 
   ```
- batch normalize

### 历程
LeNet-5 手写数字识别
VGG-11 ~19  卷积核心3*3更合适
ResNet  
































