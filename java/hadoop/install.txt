三台分别修改hostname与hosts
    vi /etc/hostname
    ========================================================================================================================
    bigdata0
    ========================================================================================================================

    vi /etc/hosts
    ========================================================================================================================
10.10.10.133 bigdata3
10.10.10.134 bigdata4
10.10.10.135 bigdata5
    ========================================================================================================================
    ping bigdata1

免密登录
    echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
    echo "UserKnownHostsFile /dev/null" >> /etc/ssh/ssh_config
    echo "RSAAuthentication yes" >> /etc/ssh/sshd_config
    ssh-keygen -t ECDSA
三台相互拷贝
    ssh-copy-id bigdata3
    ssh-copy-id bigdata4
    ssh-copy-id bigdata5

jdk与hadoop
    cd /opt
    tar -zxvf jdk-8u192-linux-x64.tar.gz
    tar -zxvf hadoop-2.7.7.tar.gz
两个配置文件都加上
    vi /etc/profile
    vi /opt/hadoop-2.7.7/etc/hadoop/hadoop-env.sh
    ========================================================================================================================
export HADOOP_HOME=/opt/hadoop-2.7.7
export SPARK_HOME=/opt/software/spark-3.3.0-bin-without-hadoop
export JAVA_HOME=/opt/jdk1.8.0_192
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=$JAVA_HOME/lib:$JAVA_HOME/jre/lib
export PATH=$JAVA_HOME/bin:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$SPARK_HOME/bin:$SPARK_HOME/sbin:$PATH
export HDFS_NAMENODE_USER=root
export HDFS_DATANODE_USER=root
export HDFS_SECONDARYNAMENODE_USER=root
export YARN_RESOURCEMANAGER_USER=root
export YARN_NODEMANAGER_USER=root
    ========================================================================================================================
hadoop配置
    vi /opt/hadoop-2.7.7/etc/hadoop/core-site.xml
    ========================================================================================================================
        <property>
            <name>fs.defaultFS</name>
            <value>hdfs://bigdata0:8020</value>
        </property>
        <!-- 指定 hadoop 数据的存储目录 -->
        <property>
            <name>hadoop.tmp.dir</name>
            <value>/home/hadoop/tmp</value>
        </property>
        <!-- 配置 HDFS 网页登录使用的静态用户为 atguigu -->
        <property>
            <name>hadoop.http.staticuser.user</name>
            <value>atguigu</value>
        </property>
    ========================================================================================================================

    vi /opt/hadoop-2.7.7/etc/hadoop/hdfs-site.xml
    ========================================================================================================================
    <!-- NameNode web 端访问地址-->
    <property>
        <name>dfs.namenode.http-address</name>
        <value>bigdata0:50070</value>
    </property>
    <!-- SecondaryNameNode web 端访问地址-->
    <property>
        <name>dfs.namenode.secondary.http-address</name>
        <value>bigdata2:9868</value>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>
    <property>
        <name>dfs.webhdfs.enabled</name>
        <value>true</value>
    </property>
    ========================================================================================================================

    vi /opt/hadoop-2.7.7/etc/hadoop/mapred-site.xml
    ========================================================================================================================
    <property>
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
    </property>
    ========================================================================================================================

    vi /opt/hadoop-2.7.7/etc/hadoop/yarn-site.xml
    ========================================================================================================================
    </property>
      <name>yarn.nodemanager.aux-services</name>
      <value>mapreduce_shuffle</value>
    </property>
    <property>
      <name>yarn.resourcemanager.hostname</name>
      <value>bigdata0</value>
    </property>

    <!-- 环境变量的继承 -->
    <property>
        <name>yarn.nodemanager.env-whitelist</name>
        <value>JAVA_HOME,HADOOP_COMMON_HOME,HADOOP_HDFS_HOME,HADOOP_CONF_DIR,CLASSPATH_PREPEND_DISTCACHE,HADOOP_YARN_HOME,HADOOP_MAPRED_HOME</value>
    </property>
    ========================================================================================================================
 vi workers
========================================================================================================================
bigdata0
bigdata1
bigdata2
========================================================================================================================
拷贝到其他两个机器
    cd /opt
    scp -r jdk1.8.0_192/ root@bigdata1:/opt
    scp -r jdk1.8.0_192/ root@bigdata2:/opt
    scp -r hadoop-2.7.7/ root@bigdata1:/opt
    scp -r hadoop-2.7.7/ root@bigdata2:/opt

    scp /etc/profile root@bigdata1:/etc/
    scp /etc/profile root@bigdata2:/etc/

三台同时
    source /etc/profile
    mkdir -p  /home/hadoop/tmp
    systemctl stop firewalld
    systemctl disable firewalld
    ========================================================================================================================
master运行
    hdfs namenode -format #仅首次
    start-dfs.sh
    start-yarn.sh

访问
    http://10.10.10.110:8088/cluster
    http://bigdata0:50070/explorer.html#/
重新配置时删除hadoop home 下的logs与配置的tmp.dir文件夹的内容并重新hdfs namenode -format


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
cd /opt/hbase-2.4.16/conf
vi hbase-site.xml
========================================================================================================================
    <!-- 指定hbase在HDFS上存储的路径 -->
    <property>
        <name>hbase.rootdir</name>
        <value>hdfs://bigdata3:8020/hbase</value>
    </property>
    <!-- 指定hbase是否分布式运行 -->
    <property>
        <name>hbase.cluster.distributed</name>
        <value>true</value>
    </property>
    <!-- 指定zookeeper的地址，多个用“,”分割 -->
    <property>
        <name>hbase.zookeeper.quorum</name>
        <value>bigdata3,bigdata4,bigdata5:2181</value>
    </property>
    <!--指定hbase管理页面-->
    <property>
      <name>hbase.master.info.port</name>
      <value>60010</value>
    </property>
    <!-- 在分布式的情况下一定要设置，不然容易出现Hmaster起不来的情况 -->
    <property>
        <name>hbase.unsafe.stream.capability.enforce</name>
        <value>false</value>
    </property>
========================================================================================================================
ln -s /opt/hadoop-3.2.4/etc/hadoop/core-site.xml  /opt/hbase-2.4.16/conf/core-site.xml
ln -s /opt/hadoop-3.2.4/etc/hadoop/hdfs-site.xml  /opt/hbase-2.4.16/conf/hdfs-site.xml
vi regionservers
========================================================================================================================
bigdata3
bigdata4
bigdata5
========================================================================================================================

cd /opt/apache-zookeeper-3.6.4-bin/conf/
cp zoo_sample.cfg zoo.cfg
vi zoo.cfg #添加以下
========================================================================================================================
dataDir=/tmp/zookeeper
dataLogDir=/tmp/zookeeper/log
========================================================================================================================
各个节点分别启动
zkServer.sh start
vi /opt/hbase-2.4.16/conf/hbase-env.sh  #添加java home
========================================================================================================================
export JAVA_HOME=/opt/openlogic-openjdk-8u262-b10-linux-64
========================================================================================================================

yum install -y rsync  #scp没有软链的特殊处理
rsync -a /opt/ bigdata4:/opt/
rsync -a /opt/ bigdata5:/opt/

start-hbase.sh #仅master

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
yum install postgresql-server
postgresql-setup initdb
cd /var/lib/pgsql/data
vi pg_hba.conf
==========================
local all all peer
#修改为:
local all all trust
#添加一行
host    all             all             0.0.0.0/0               trust
==========================
vi postgresql.conf
===============================
listen_addresses = '*'
===============================

service postgresql restart

psql -U postgres
CREATE USER hive WITH PASSWORD 'hive';
create database hive;
grant all privileges ON DATABASE hive to hive;

rm /opt/apache-hive-3.1.3-bin/lib/guava-19.0.jar
cp /opt/hadoop-3.2.4/share/hadoop/common/lib/guava-27.0-jre.jar /opt/apache-hive-3.1.3-bin/lib/

cd /opt/apache-hive-3.1.3-bin/bin/
./schematool -dbType postgres -initSchema

同步另外两台机器
rsync -a /opt/ bigdata4:/opt/
rsync -a /etc/profile bigdata4:/etc/profile

vi /opt/apache-hive-3.1.3-bin/conf/hive-site.xml
=============================== 额外配置
        <property>
                <name>hive.metastore.uris</name>
                <value>thrift://bigdata3:9083</value>
        </property>
===============================








