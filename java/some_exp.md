## 使用ImageIO相关类，打包后运行报错如下

```
java.util.ServiceConfigurationError: javax.imageio.spi.ImageOutputStreamSpi: Provider com.sun.media.imageioimpl.stream.ChannelImageOutputStreamSpi could not be instantiated
        at java.util.ServiceLoader.fail(ServiceLoader.java:232)
        at java.util.ServiceLoader.access$100(ServiceLoader.java:185)
        at java.util.ServiceLoader$LazyIterator.nextService(ServiceLoader.java:384)
        at java.util.ServiceLoader$LazyIterator.next(ServiceLoader.java:404)
        at java.util.ServiceLoader$1.next(ServiceLoader.java:480)
        at javax.imageio.spi.IIORegistry.registerApplicationClasspathSpis(IIORegistry.java:210)
        at javax.imageio.spi.IIORegistry.<init>(IIORegistry.java:138)
        at javax.imageio.spi.IIORegistry.getDefaultInstance(IIORegistry.java:159)
        at javax.imageio.ImageIO.<clinit>(ImageIO.java:66)
        at tilecreator.utils.task.TileCreteTaskRunnable.getImageByteArrayResource(TileCreteTaskRunnable.java:521)
        at tilecreator.utils.task.TileCreteTaskRunnable.writeToStore(TileCreteTaskRunnable.java:415)
        at tilecreator.utils.task.TileCreteTaskRunnable.processTile(TileCreteTaskRunnable.java:291)
        at tilecreator.utils.task.TileCreteTaskRunnable.run(TileCreteTaskRunnable.java:174)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
        at java.lang.Thread.run(Thread.java:748)
Caused by: java.lang.IllegalArgumentException: vendorName == null!
        at javax.imageio.spi.IIOServiceProvider.<init>(IIOServiceProvider.java:76)
        at javax.imageio.spi.ImageOutputStreamSpi.<init>(ImageOutputStreamSpi.java:91)
        at com.sun.media.imageioimpl.stream.ChannelImageOutputStreamSpi.<init>(ChannelImageOutputStreamSpi.java:64)
        at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
        at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
        at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
        at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
        at java.lang.Class.newInstance(Class.java:442)
        at java.util.ServiceLoader$LazyIterator.nextService(ServiceLoader.java:380)

Exception in thread "pool-1-thread-617" java.lang.NoClassDefFoundError: Could not initialize class javax.imageio.ImageIO
        at tilecreator.utils.task.TileCreteTaskRunnable.getImageByteArrayResource(TileCreteTaskRunnable.java:521)
        at tilecreator.utils.task.TileCreteTaskRunnable.writeToStore(TileCreteTaskRunnable.java:415)
        at tilecreator.utils.task.TileCreteTaskRunnable.processTile(TileCreteTaskRunnable.java:291)
        at tilecreator.utils.task.TileCreteTaskRunnable.run(TileCreteTaskRunnable.java:174)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
        at java.lang.Thread.run(Thread.java:748)
```

-  maven配置中缺少vendorName相关配置
```xml
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.5.5</version>
                <configuration>
                    <archive>
                        <manifest>
							<addClasspath>true</addClasspath>
                            <mainClass>tilecreator.cutter</mainClass>
                        </manifest>
                        <manifestEntries>
                            <Implementation-Title>com.sun.media.imageio</Implementation-Title>
                            <Implementation-Version>1.1</Implementation-Version>            
                            <Implementation-Vendor>Sun Microsystems, Inc.</Implementation-Vendor>           ！！！！！！！！重点
                            <Implementation-Title>com.sun.media.imageio</Implementation-Title>
                        </manifestEntries>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>tilecreator.cutter</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
```



===================================================================================================
###repackage failed: Unable to find main class

一、Maven打包Spring Boot项目报错（repackage failed: Unable to find main class），排除寻找Main方法，一般用于被依赖的公用常量模块，解决方法如下：

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <mainClass>none</mainClass>     <!-- 取消查找本项目下的Main方法：为了解决Unable to find main class的问题 -->
                <classifier>execute</classifier>    <!-- 为了解决依赖模块找不到此模块中的类或属性 -->
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```
二、其他服务模块可以正常配置打包插件如下：
```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>
  </plugins>
</build>
```

===================================================================================================
### git tar 打包后 window 解压 文件有修改
```sh
git config core.filemode false
```


===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================



===================================================================================================