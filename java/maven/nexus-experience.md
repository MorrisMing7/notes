##### 准备
- nexus-3.75.1-01-unix.tar.gz
https://sonatype-download.global.ssl.fastly.net/repository/downloads-prod-group/3/nexus-3.75.1-01-unix.tar.gz 
- openlogic-openjdk-17.0.13+11-linux-x64.tar.gz
https://builds.openlogic.com/downloadJDK/openlogic-openjdk/17.0.13+11/openlogic-openjdk-17.0.13+11-linux-x64.tar.gz

##### 解压后
编辑 start.sh
```sh
export JAVA_HOME=/home/nexus/openlogic-openjdk-17.0.13+11-linux-x64
export PATH=$JAVA_HOME/bin:$PATH
java -version
cd nexus-3.75.1-01/bin/
./nexus start
```
##### 日志
sonatype-work/nexus3/log/

##### 创建host类型私服
![alt text](image.png)
##### 将host添加到public中
![alt text](image-1.png)

##### 有关 maven-metadata.xml
![alt text](image-2.png)

##### 在pom中添加私服
```xml
<repositories>
    <repository>
        <id>mavenXXXa</id> <!--注意这里不能使用 'maven250'之类的字眼 -->
        <url>http://192.168.10.250:8081/repository/maven-public/</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
<pluginRepositories>
    <pluginRepository>
        <id>mavenXXXa</id>
        <url>http://192.168.10.250:8081/repository/maven-public/</url>
    </pluginRepository>
</pluginRepositories>
```




#### 导入本地数据到私服
编辑 *mavenimport.sh*
```sh
#!/bin/bash
# copy and run this script to the root of the repository directory containing files
# this script attempts to exclude uploading itself explicitly so the script name is important
# Get command line params
while getopts ":r:u:p:" opt; do
        case $opt in
                r) REPO_URL="$OPTARG"
                ;;
                u) USERNAME="$OPTARG"
                ;;
                p) PASSWORD="$OPTARG"
                ;;
        esac
done

find . -type f -not -path './mavenimport\.sh*' -not -path '*/\.*' -not -path '*/\^archetype\-catalog\.xml*' -not -path '*/\^maven\-metadata\-local*\.xml' -not -path '*/\^maven\-metadata\-deployment*\.xml' -not -path 'settings*.xml' | sed "s|^\./||" | xargs -I '{}' curl -u "$USERNAME:$PASSWORD" -X PUT -v -T {} ${REPO_URL}/{} ;
```
将此文件放入本地仓库目录，使用以下命令  
`./mavenimport.sh -u admin -p 123456 -r http://192.168.10.250:8081/repository/daoda1/`
