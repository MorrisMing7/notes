## why async
### 几种常见并发模型
- 系统线程：线程间同步困难，线程切换开销大，虽然线程池会减小切换开销，但对于大量IO的场景仍显不足
- 事件驱动编程： 数据流与错误传递往往跟踪困难
- 协程(Coroutines): 与线程类似，亦是没有对编程模型的改变；与async相比也支持大量任务，但是其抽象隐藏了对于系统编程与运行时实现相关的底层细节
- actor model: 将并发计算划为单元，通过与分布式系统类似的消息来沟通彼此；此模型实现很有效率，但是对于流控制与重试等等问题没有解决
### x
相比系统线程，async更适用于大量IO操作的场景像是服务或数据库；    
如果不是性能需要，os thread更为简单
```rust
fn use_thread(){
    let t1 = thread::spawn(download("www.foo.com"));
    let t2 = thread::spawn(download("www.bar.com"));
    t1.join().expect("thread 1 panicked");
    t2.join().expect("thread 2 panicked");
}
async fn use_async(){
    let future_1 = download_async("www.foo.com");   //下载页面只是一个很小的任务，为此创建线程得不偿失
    let future_2 = download_async("www.bar.com");
    join!(future_1,future_2);
}
```

### 兼容问题
不能在非异步函数中调用异步函数；    
即使都是异步函数，一些crate依赖于特定的异步运行时，不能混用
