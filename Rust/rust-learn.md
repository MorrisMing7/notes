## cargo
- `cargo build `  
  会生成`Cargo.lock`文件记录文件变化  
  在target/debug下生成可执行程序
- `cargo run`    
  执行程序，会自动build
- `cargo check`
  检查编译问题
- `cargo build --release` 更多编译优化
- `cargo update` 忽略 Cargo.lock文件中的版本，重新计算由Cargo.toml确定的最新依赖
### cargo 依赖
在Cargo.toml中的`[dependencies]`下添加`rand = "0.8.3"`
> Cargo understands Semantic Versioning (sometimes called SemVer). 
> The number 0.8.3 is actually shorthand for ^0.8.3
> Cargo considers these versions to have public APIs compatible with version 0.8.3     

默认都下载兼容的最新版，在最初一次build中Cargo会将依赖的版本写入`Cargo.lock`，
后续再次build就不会再次下载最新版。故常常将Cargo.lock文件和代码**一起提交版本控制**。


- 猜数游戏
  ```rust
  use std::{io, cmp::Ordering};
  use rand::Rng;
  
  fn main(){
      let rand_num = rand::thread_rng().gen_range(1..=100);
      print!("random num:{rand_num}");
      loop{
          println!("input: ");
          let mut input = String::new();
          io::stdin()
          .read_line(&mut input)
          .expect("failed to read line");
          let input: u32 = match input.trim().parse() {   //重复声明并转型
              Ok(num) => num,
              Err(_) => {             //_表示匹配所有Err变量
                  print!("input not a num");
                  continue;
              }
          };
          match input.cmp(&rand_num){
              Ordering::Less => print!("small "),
              Ordering::Equal => {
                  print!("bingo ");
                  break;
              },
              Ordering::Greater => print!("big "),
          }
          //println!("your input:{input},rand num:{rand_num}")
      }
  }
  ```

##一些通用概念

### 变量    
- 变量不声明为可变的，均为不可变
  ```rust
    fn compile_error(){
        let x = 5;
        x = 6; //无法编译
    }
  ```
- 使用`mut`关键字声明变量可变
  ```rust
    fn main(){
        let mut x = 5;
        x = 5*x;
    }
  ```
#### shadowing
ps: 相当于重声明，可以进行类型转换
  ```rust
    fn main(){
        let x = 5;
        let x = x+1;
        {
            let x = x*2;
            print!("in bracket x is {x}");
        }
        print!("out of braket x is {x}");
    }
  ```
  ```rust
    fn main(){
        let x = "5";
        let x:i32 = x.parse().expect("");
    }
  ```
  mut关键字就不能直接用于类型转换
  ```rust
    fn compile_error(){
        let mut spaces = "xxxx";
        spaces = x.len();  //编译错误 
    }
  ```
### 常量
- 使用`const`关键字声明常量，同时必须标记类型
  ```rust
  const WEEK_IN_SECONDS: u32 = 7 * 24 * 60 * 60;
  ```

### 变量类型
#### scalar type 
基本类型

- 整型

  | Length	| Signed	| Unsigned |
  | ---- | ---- | ---- |
  | 8-bit		| i8		| u8	|
  | 16-bit	| i16		| u16 |
  | 32-bit	| i32	    | u32|
  |64-bit	    |i64	    |u64|
  |128-bit	|i128	    |u128|
  |arch(架构决定）	|isize	|usize|
  - 每种有符号的表示范围即补码表示的范围$ [-2^(n - 1) ~ 2^(n - 1) - 1] $
  - 数字可以使用下划线做分割符使长数字可读性更高 如`1_000_000`
  - 可以使用后缀表示类型如 `23u8`
  - overflow
    如果存储超过限制，如u8类型存储值256，在debug模式下会*cause your program to panic at runtime*
    在release模式下编译，*if overflow occurs, Rust performs 'two’s complement wrapping'*，即会保留原始大小的部分，
    如u8类型存储256，值会为0，存储257会为1。不要使用这种行为特性。
- 浮点型
  - f64
  - f32    
  默认为f64,符合*IEEE-754* 标准    
  ```rust
    fn main(){
        let x = 2.3; //f64
        let y: f32 = 2.333;  //f32
    }
  ```
- 布尔型`bool`
  使用关键字bool区分，占**一个**字节
  ```rust
    fn main(){
        let t = true;
        let x: bool = false;
    }
  ```
- 字符型，**4字节**
  ```rust
    fn main(){
        let c = 'z';
        let z: char = 'ℤ'; // with explicit type annotation
        let heart_eyed_cat = '😻';
    }
  ```
#### 复合类型
- 元组
  ```rust
    fn main(){
      let tup: (u8,f32,bool) = (128,2.33,true);
      let (x,y,z) = tup;
      println!("y is {y}");   //2.33
      let z = tup.2;          //使用.n
      println!("z is {z}")
    }
  ```
  - 可以使用`.n`来获取元组中第n个数据，**从0开始**
  - 空的元组`()`被称为'unit' ,如果表达式不返回任何东西，则代表隐式的返回一个unit
- 数组
  ```rust
    fn main(){
        let a = [1,2,3,4,5];
        let a: [i32;5] = [1,2,3,4,5];
        let a = [3;5];
    }
  ```
  - 数组中的元素类型必须相同
  - 数组长度是固定的
  - 可以使用`[类型;长度]`的方式显示声明类型    
    `
    let a: [i32;5] = [1,2,3,4,5];
    `
  - 可以简单的声明重复数据的一个数组
    ```
    let a = [3;5];
    //等价于
    let a = [3,3,3,3,3,3];
    ```
  - 使用下标访问数组时，rust会检查长度
    ```rust
    fn main(){
        let a = [3;5];
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("failed to read");
        let input:usize = input.trim().parse().expect("not a num");
        let element = a[input];
        println!("the {input}th num of array is {element}");
    }
    ```
### 控制流
#### 函数
  ```rust
  fn my_some_function(arg1:u32,arg2:bool){
    //do something
  }
  ```
  - 声明与表达式    
    声明*Statements*没有返回值，表达式*Expressions*有返回值 
    ```
    let y = 6;   //声明
    let x = (let y = 7);   //编译错误 声明没有返回值
    let z = y = 5;   //会报y不可修改的错误，修改y为mut后，z会是一个unit类型，
    ```
  - **不要**使用 `x= y= 127` 这种形式接连赋值，x没有正确赋值
  - 花括号是一个表达式
    ```
    let y = {
        let x = 3;
        println!("x is {x}");
        x+1    //没有';'号
    };
    println!("y is {y}");   //4
    ```
    ```rust
    fn plus_one(num:i32)-> i32{
      num +1   //没有;号
    }
    ```
  - 返回值
#### 条件分支 if   
  ```
    let x = 2;
    if x==2 {
        println!("x is 2");
    } else if x<2{
        println!("x is less than 2");
    } else{
        println!("x is bigger than 2");
    }
  ```
  - 声明中使用if，各个块表达式返回值**类型必须一致**
  ```
    let b = true;
    let x = if b {1} else {0};
    let y = if b {233} else {"false"};  //编译错误
  ```
#### 循环
- `break`可以返回值
  ```rust
    fn main(){
        let mut counter = 0;
        let result = loop {
            counter += 1;
            if counter == 10 {
                break counter * 2;
            }
        };
        println!("{result}")
    }
  ```
- 可以使用`continue` `break` 关键字
- 可以在嵌套的循环中使用*label*
  ```rust
    fn main(){
        let mut x = 0;
        'outter: loop{
            println!("x is {x}");
            let mut y: u8 = 9;
            loop{
                println!("y is {y}");
                if y<7 {
                    break;
                }
                if x>2 {
                    break 'outter;    //跳出外层循环
                }
                y = y-1;
            }
            x = x+1;
        }
    }
  ```
- `while`条件循环
  ```rust
    fn main(){
        let mut x = 0;
        while x<3 {
            println!("now x is {x}");
            x += 1;
        }
    }
  ```
- `for`遍历数组
  ```rust
    fn main(){
        let x = 4;
        for element in (1..x).rev() {  //Range
            println!("{element}")
        }
    }
  ```
  - enumerate
  ```rust
    fn main(){ 
        let s = String::from("hello world!");
        let i = first_word_idx(&s);
        println!("{i}");
    }  
    fn first_word_idx(s: &String) ->usize{
        for (i,&item) in        //enumerate返回的是引用类型，故这里用一个&代表取其值
            s.as_bytes().iter().enumerate(){
            if item == b' '{
            return i;
            }
        }
        return s.len();
    }
  ```
## *ownership*
- 堆与栈
  所有分配在栈上的数据都是已知并固定大小的，其他则被分配在堆上
所有权遵循以下原则
> - 所有变量都有主人(owner)
> - 同一时间主人只有一个
> - 主人离开作用域变量即被丢弃
- 所有权转移，分配在堆上的变量会转移所有权
  ```rust
    fn main(){
        let x= 5;
        let x2 = x;           //栈上的变量
        println!("{x}");      //ok
        let s = String::from("hello");
        let s2 = s;           //s、s2包含了堆上指针
        println!("{s}");       //编译错误  s的值已经给了s2，为避免离开作用域后指向同一位置的堆上资源被重复释放，s复制给s2后即失效
    }
  ```
- Stack-Only Data: copy  
  - 对于栈上的变量，若类型实现了*copy*特性，则它在赋值给另一个变量后*依旧有效*
  - 类型或其中的部分实现了*drop*特性的不能再实现copy
  - 实现copy特性的类型： 整型、浮点型、布尔型、字符型、以及所有类型都实现copy的元组类型

### 函数与所有权
- 交给函数的变量
  ```rust
    fn main(){
        let s = String::from("hello");  
        takes_ownership(s);     // s所有权进入函数，会在函数内被释放...
                                // ... 函数后s变量即失效
        let x = 5;                      
        makes_copy(x);          // copy特性使得x在函数后继续有效
    }
  ```
- 所有权返回
  ```rust
    fn main() {
      let s2 = String::from("hello");     // s2 comes into scope
      let s3 = takes_and_gives_back(s2);  
    }
    fn takes_and_gives_back(a_string: String) -> String {
      a_string  // a_string is returned and moves out to the calling function
    }
  ```
- 引用, 传值而不转移所有权
  ```rust
    fn main(){
        let x = String::from("hello");
        let y = the_length(&x);
        println!(" string '{x}' length is {y}");
    }
    fn the_length(str:&String)->u32{
        return str.len();
    }
  ```
  创建一个引用的行为被称为***borrow***，即所有权不会变化
  
- 引用的修改，借来的变量没有标记为可修改的则不可修改
  ```rust
    fn main(){
      let mut s = String::from("hello");
      change(s);            
    }
    fn change(s: &String){
      s.push_(", world");            //错误，借来的变量没有标记为可修改的则不可修改
    }
  ```
  ```
    //正确版本
    fn change(s: &mut String){       
        s.push_str(", rust");   
    }
  ```
  - 可变的引用同一时间只能有一个
  ```rust
    fn main(){
        let mut s = String::from("hello");
        let s1 = &mut s;          //可变引用1 
        let s2 = &mut s;          //可变引用2，编译错误： second mutable borrow occurs here
        println!("{s1},{s2}");
    }
  ```
  - 引用的作用范围从声明开始到最后一次使用结束，若自己不可变引用的话，则还可以继续声明不可变引用
  ```rust
    fn main(){
        let mut s = String::from("hello");
        let s1 = &s;          //非可变引用1 
        let s2 = &s;          //非可变引用2，编译错误： second mutable borrow occurs here
        println!("{s1},{s2}");
        let s3 = &s2;
    }
  ```
- 悬挂引用，函数是不能返回引用类型的
  ```rust
    fn dangle()-> &String{    //编译错误
        let s = String::from("xxxx");
        &s                    //需要返回的直接返回所有权即可 即 return s;
    }                //离开作用域后s即被释放
  ```
### *Slice Type*
`some_arg[starting_index..ending_index]` 一个字符串的切分即其中一段的引用
```rust
    fn string_slice(){
        let s = String::from("hello, world");
        let first_world = &s[0..5];      //也可以写作 s[..5]
        let second_world = &s[6..s.len()];   //也可以写作 s[6..]
    }
```
  - 对于字符串的切分，若边界落在utf-8字符直接，程序会错误退出
  - String的 slice类型可以简写为`&str`
  ```rust
    fn first_world(s: &String) -> &str{
        for (i,&item) in s.as_bytes().iter().enumerate(){
            if item == b' '{
                return &s[..i];
            }
        }
        return &s[..];
    }
  ```
  - 以文本形式表示的String类型是String slice的引用类型
  ```rust
    fn mian(){
      let str1 = String::from("hello, world");
      let str_literal = "hell, rust";
      let s1 = first_world(&str1);
      let s2 = first_wrold(str_literal);    //literal的字符串是一种String slice引用
    }
    fn first_world(s: &str) -> &str{    //入参改为 &str类型
        return &s[..];
    }
  ```
 - 其他版本的slice
  ```rust
    fn main(){
        let a = [1,2,3,4,5];
        let a_slice = &a[1..3];
        assert_eq!(&a_slice,&[2,3]);
    }
  ```
## 结构体
```rust
  struct Student{
      name:String,
      gender:bool,
      age:u8,
      active:bool,
      sign_in_cout: u32,
      email:String,
  }
```
#### 改变结构体中的数据要标记`mut`,
  - rust不允许结构体中仅部分字段可变，要变全变
    ```rust
    fn main(){
        let mut stu1 = Student{
            name:String::from("morris"),gender:true,age:18,active:true,sign_in_cout:0,
            email:String::from("xxx@xx.com"),
        };
        stu1.name = String::from("张三");     //可变结构体中的所有字段均可变
    }
    ```
#### 使用其他结构体初始化
  - 行尾不能有`,`号，且必须在结构体最后一行
  - 这种方式初始化相当于使用 `=`进行赋值操作，变量会move到新的结构体中，`stu1`中的变量即失效
    ```rust
      fn main(){
        let stu1 = xxxxxxx;//同上    
        let stu2 = Student{ 
            active:false,     //个别变量可以单独赋值     
            ..stu1            //使用其他结构体初始化，
        };
        let str = stu1.name;  //编译错误，`use of moved value`
      }
    ```
  - 变量名与字段名一致时可以简写
    ```rust
      fn create_student(name:String,gender:bool,age:u8,email:String) -> Student{
          return Student{
              name,gender,age,email,
              active:true,
              sign_in_cout:0,
          };
      } 
    ```
#### 元组结构体
  ```rust
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);
    fn main() {
        let black = Color(0, 0, 0);
        let origin = Point(0, 0, 0);
    }
  ```
    - 即使变量类型完全一致，`Color`与`Point`也是不同的类型
#### 示例
  - 集成Debug特性， `#[derive(Debug)]`,
  - 使用 `{:?}`或者 `{:#?}`格式化输出结构体
    ```rust
      fn main(){  
          let rect = Rectangle{width:32,height:22};
          println!("rectangle is : {:?}",rect);     
      }  
      #[derive(Debug)]                   
      struct Rectangle {
          width: u32,
          height: u32,
      }
    ```
  - 宏命令 `dbg!(xxx)`
    - 会拿走所有权
    ```rust
    fn main(){
      let scale = 4;
      let rect = Rectangle{
          width:32,
          height:dbg!(scale* 22)
      };
      dbg!(&rect);      //使用引用避免交付所有权
    }
    ```
#### 方法
  - 使用impl实现结构体方法
    ```rust
      #[derive(Debug)]
      struct Rectangle{width:u32,height:u32}
      impl Rectangle {
          fn area(&self)->u32{
              return self.width * self.height;
          }
      }
    ```
  - 方法可以与字段同名
    ```rust
      impl Rectangle {
          fn width(&self)->bool {
              return self.width!=0;
          }
      }
      fn main(){
          let r = Rectangle{width:34,height:13};
          if r.width(){
              println!("width is nonzero")
          }
      }
    ```
  - 多个参数方法 `Self`关键字
    ```rust
      impl Rectangle{
          fn can_hold(&self,other:&Rectangle)->bool{
              return self.width>other.width && self.height > other.height;
          }
          fn square(size:u32)->Rectangle{           //可替换为 `Self`
              Self { width: size, height: size }    //等价于Rectangle
          }
      }
      fn main(){
          let r = Rectangle{width:34,height:13};
          let r2 = Rectangle::square(12);
          if r.can_hold(&r2){
              println!("r can hold r2")
          }
      }
    ```

## 枚举与模式匹配 
Enums  Pattern Matching
  ```rust
    enum Color{
      Red,
      Blue{r:u8,g:u8,b:u8},
      White(f32),
      Black(bool)
    }
  ```
- 类似于
  ```rust
    struct RedColor;
    struct BlueColor{r:u8,g:u8,b:u8}
    struct White(f32);
    struct BlackColor(bool);
  ```
- 匹配
  - 必须覆盖所有值
  ```rust
    fn main(){
        let x = Color::Blue {r:3,g:4,b:5};
        x.say();
        match x{
            Color::Red => println!("red"),
            Color::Blue => println!("Blue"),
            Color::White(3.2) => println!("White"),
            Color::White(2.2) => println!("White"),
            _ => {println!("ooooh")}        //默认分支
        }
    }
  ```
  ```rust
    enum Planet{
        Mars, Earth, Jupiter,
    }
    enum TimeSpan{
        Second, Day(Planet), Year(Planet),
    }
    fn how_long(time:TimeSpan,planet:Planet){
        match time {
            TimeSpan::Day(Planet::Earth)=>{println!("24h");},
            TimeSpan::Year(Planet::Mars)=>println!("11.82years"),
            other =>{println!("no idea")}
        }
    }
  ```
  特殊值
  ```rust
  fn main(){
      let x = Some(2233);
      let y = 13;
      match x{
          Some(3) => {println!("match 3")},
          Some(2233) => {println!("match 2233")},
          Some(y) => {println!("match {y}")},     //y是重新声明的变量而非13
          None => {println!("none")},
      }
  }
  ```
  多个匹配
  ```rust
  fn main(){
      let x =1;
      match x{
          1|2 =>{println!("1 or 2")},
          3 => {println!("3")},
          _ => {println!("other")}
      }
  }
  ```
  区间匹配 仅适用于数值与char型
  ```rust
  fn main(){
      let x =1;
      match x{
          1..=5 =>{println!("num is in [1,5]")},
          8 => {println!("8")},
          _ => {println!("other")}
      }
  }
  ```
- `option<T>`
  ```rust
    fn main(){
        let x = Some(5);
        let y = plus_one(x);
        match y {
            Some(i)=>{println!("{i}")}
            None =>{ print!("None");}
        }
    }
    fn plus_one(x:Option<i32>)->Option<i32>{
        return match x{
            None => {None}
            Some(i) => {Some(i+1)}
        };
    }
  ```
- 默认分支什么都不做
  ```rust
    fn dice(){
        let x = 5;
        match x {
            3 => add_fancy_hat(),
            7 => remove_fancy_hat(),
            _ => (),
        }  
    }

  ```
### if let
  ```rust
  fn main(){
      let my_color = Color::Blue {r:1,g:2,b:3};
      if let Color::Blue {r,g,b} = my_color{
          println!("this is a Blue by r{r} g{g} b{b} ")
      }
  }
  ```
- 默认分支
  ```rust
  fn main(){ 
      let my_planet = Planet::Mars;
      if let Planet::Earth = my_planet{
          println!("welcome home");
      }else {
          println!("welcome to Earth");
      }
      //等价于
      match my_planet{
          Planet::Earth => println!("welcome home"),
          _ => println!("welcome to Earth")
      }
  }
  ```
### while let
  ```rust
  fn main(){
      let mut v = vec![2,3,4];
      while let Some(x) = v.pop(){
          println!("{x}")
      }
  }
  ```
### 解构
  ```rust
  struct Point{x:i32,y:i32}
  fn main(){
      let p1 = Point{x:2,y:3};
      let Point{x,y} = p1;
      println!("{x}-{y}");
  }
  ```
  ```rust
  fn main(){
      let p1 = Point{x:2,y:3};
      match p1{
          Point{x:0,y} => println!("x is 0,y is {y}"),
          Point{x,y:0} => println!("y is 0, x is {x}"),
          _ =>(),
      }
  }
  ```
元组类似，可以多层匹配
  ```rust
  enum Msg{
      Quit,
      ChangeColor(Color),
      Write(String),
  }
  enum Color {
      RGB(i32,i32,i32),
      HSV(i32,i32,i32)
  }
  fn main(){
      let p1 = Msg::ChangeColor(Color::HSV(1, 2, 3));
      match p1{
          Msg::Quit => todo!(),
          Msg::ChangeColor(Color::HSV(1,s,v)) => todo!(),     //可以多层
          Msg::ChangeColor(Color::RGB(1,g,b)) => todo!(),
          Msg::Write(text) => todo!(),
          _=>(),
      }
  }
  ```
- 使用`_`与`..`表示忽略值
  ```rust
  fn main(){
      let x = (1,2,2,3,45,2);
      let (_, second, .., last) =x;
      println!("{second}-{last}")
  }
  ```
### 额外的匹配条件
  ```rust
  fn main(){
      let x = Some(2233);
      let y = 13;
      let a = true;
      match x{
          Some(3)|Some(2233) if a  => {println!("match 3")},
          Some(v) if v==y => {println!("match {y}")},     //y是重新声明的变量而非13
          None => {println!("none")},
          _=>()
      }
  }
  ```
- 绑定值
  ```rust
  enum Msg{
      Quit,
      Color {r:i32,g:i32,b:i32},
      Write(String),
  }
  fn main(){
      let x = Msg::Color{r:12, g:23, b:12};
      match x{
          Msg::Color{r:90..=100, ..} => print!("r in 90-100"),
          Msg::Color { r:red@10..=90, g, b }=>
              println!(" r in 10-90,{red}-{g}-{b}"),
          _=>()
      }
  }
  ```
## 模块系统
### packages与crates
- package是cargo功能，用以构建测试分享crates
  - 一个package包含一个Cargo.toml文件
  - `cargo new` 出的项目，默认包含*src/main.rs* 文件，作为 binary crate的root file；同样地，*src/lib.rs*作为library crate的根
  - 若同时包含*main.rs*与*lib.rs*，则package就包含两个crate名称与package同名
- crates: modules构成的树，用以生成库或可执行文件，最小编译单位
    > create 分为二进制binary crate与库library crate两种形式    
    > library crate没有main函数，专为共享其中实现的函数而使用
### modules与path
> **modules**和**use**: 用以控制path的作用域和私有性    
**path**: 结构体、函数或module的命名方式
- 可以使用 `mod garden`的形式声明一个模块
- 被声明的模块garden在该文件的同目录下的*garden.rs*或*garden/mod.rs*(old style)中，结构可以如下所示
    ```txt
    src/
      main.rs       ##声明 mod garden
      garden.rs     ##声明 mod vegetable
      garden/
        vegetable.rs
    ```
- `pub` 某个类型或函数需要被模块外可见时就需要此关键字声明,子模块可以看到父模块的path，反之则不行
- `use` 使用此关键字可以简化path的长度
  - 子模块不能继承父模块使用use简化的path，但是依旧可以使用`super::xxx`来简化path
  ```rust
    use garden::vegetable;
    mod my_some_mod{
        fn some_fn(){
            super::vegetable::Potato;
        }   
    }
  ```
- path的两种形式
  - 绝对路径：对于外部crate，以其名称作为开头，对于当前的crate，以 `crate`关键字开头相当于文件系统中的 */*
  - 相对路径： `self` `super`
  - 
- 对于struct，字段不标记为pub则结构体外无法访问；对于enum，枚举类型默认可以被枚举外部访问；

- 对于要引入的类型含有同样名称的情况，可以使用 `as`关键字
  ```rust
    use std::fmt::Result;
    use std::io::Result as IoResult;  
  ```
- *re-exporting* : `pub use` 需要使用`crate`   
  file: *src/garden/mod.rs*
    ```rust
        mod A{
            pub use crate::garden::B::B2;
            pub mod A1{
                pub fn some_fn(){
                    
                }       
            }
        }
        mod B{
            pub mod B2{
                pub fn some_fn22(){ }
            }
        }
        use A::B2;
        fn some_fn222(){ 
            B2::some_fn22();
        }
    ```
- `use`的嵌套
  ```rust
    use std::cmp::Ordering;
    use std::io;
    //等价于
    use std::{cmp::Ordering,io};
  ```
  ```rust
    use std::io;
    use std::io::Write;
    //等价于
    use std::{self,Write};
  ```
## 集合
### vector
- 创建时声明类型
`let v:Vec<i32> = Vec::new();`
- 类型推断
`let v = vec![1,2,3];`
- 取值
  - v.get返回一个 `Option<&T>`越界会返回None,  `[]`越界的话会panic
  ```rust
    fn main(){
        let v = vec![1,2,3];
        let vx100 = v[100];         //panic
        let vx100  = v.get(100);    //None
    }
  ```
  ```rust
    #[derive(Debug)]
    struct Node{data:i32}
    fn it_works() {
        let v = vec![Node{data:1},Node{data:2},Node{data:3}];
        let x1 = &v[2];
        println!("x is {:?}",x1);
        let x2 = v.get(2);
        if let Some(x) = x2 { println!("get 2th is {}",x.data)}
    }
  ```
- 修改
  ```rust
    fn it_works() {
        let mut v = vec![1,23,3,4];
        for i in &mut v{
            *i +=2;
        }
        for i in v{
            println!("{i}")
        } 
    }
  ```
### string
  ```rust
    fn string_update(){
        let mut s = String::from("hello ");
        let s1 = " world";
        s.push_str(s1);
        s.push('.');
        println!("{s1}");      //没有拿走s1的所有权
        println!("{s}");
  
        let s1 = String::from("oh");
        let s2 = " my";
        let s3 = String::from(" god");
        let s4 = s1 + s2 + &s3;
        //println!("{s1}");       //s1所有权已转移，报错
        println!("{s2}");
        println!("{s3}");
    }
  ```
  - 其中`+`操作是个函数    
    `fn add(self,s:&str)->String`
  - *deref coercion*     
    其中`&s3`理应是 *&String* 类型，rust编译器可以 ： *&String* -> *&str*
- `format` 不会拿走所有权
```rust
fn format_string(){
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s = format!("{}-{}-{}",s1,s2,s3);
    println!("{s}")
}
```
- String是由`Vec<u8>`实现的
- chars遍历
```rust
fn test_chars(){
  for i in "狼烟起，江山北望".chars(){
    println!("{i}")
  }
}
```
### HashMap
```rust
    fn it_works() {
        let mut map = HashMap::new();
        let k = String::from("k");
        let v = String::from("v");
        map.insert(k,v);  
        //k,v所有权被转移
    }
```
- `insert`方法会覆写掉原有值
- 校验不存在后添加
```rust
    fn it_works() {
        let mut map = HashMap::new();
        map.entry(String::from("k")).or_insert("v");
        map.entry("k".to_string()).or_insert("v2");
        println!("{:?}",map);           //v
    }
```
- 修改
```rust
    fn it_works() {
        let mut map = HashMap::new();
        let data = "hello world  this is a new world";
        for word in data.split_whitespace(){
            let count = map.entry(word).or_insert(0);
            *count +=1;
        }
        println!("{:?}",map);
    }
```
### summary
#### 
```rust
    //中位数
    fn mid(v:&mut Vec<i32>) -> Option<i32> {
        v.sort();
        let x = v.len()/2;
        if let Some(y) = v.get(x){
            return Some(*y);
        }else {
            return None;
        }
    }
    //众数
    fn most_often(v:&Vec<i32>)->Option<i32>{
        let mut map = HashMap::new();
        let mut max_count = 0;
        let mut most_often_num = None ;
        for i in v {
            let count = map.entry(i).or_insert(0);
            *count += 1;
            if max_count < *count {
              most_often_num = Some(*i);
              max_count = *count;
            }
        }
        return most_often_num;
    }
```
//职工-类型-类型统计与排序
```rust
    #[derive(PartialEq,Debug)]
    enum Prof{
        Engineer,
        Sale,
        Other
    }
    #[derive(Debug)]
    struct User{
        name:String, prof:Prof
    }

    struct Company{
        worker:Vec<User>
    }

    impl Company {
        pub fn new()->Company{
            return Company{worker:Vec::new()}
        }
        pub fn add_worker(&mut self, name:String, prof:Prof){
            self.worker.push(User{name,prof})
        }
        pub fn get_by_prof(&self, prof:Prof)->Vec<&User>{
            let mut v = Vec::new();
            for u in &self.worker{
                if prof == u.prof{
                    v.push(u);
                }
            }
            v.sort_by(|a,b|a.name.cmp(&b.name));
            return v;
        }
        pub fn print_by_prof(&self,prof:Prof){
            let v = self.get_by_prof(prof);
            for i in v{
                println!("{:?}",i);
            }
        }
    }
```








##错误处理
错误分为两种: 可恢复 与 不可恢复    
- 可恢复的错误返回`Result<T,E`>     
- 不可恢复的错误会panic停止执行

### panic的两种处理
- 可以通过两种方式触发panic
  - 代码致命错误（如数组越界）
  - 显式调用宏`panic!`
- 两种处理
  - *unwinding*    
    默认情况下，panic后程序会开始*unwinding*，即堆栈依次退出，并清理数据
  - *aborting*    
    不清理的情况下退出程序
    若需要在release模式中panic后直接aborting，则需要在Cargo.toml中配置
    ```toml
    [profile.release]
    panic = 'abort'
    ```
- 环境变量 `RUST_BACKTRACE`控制打印堆栈回溯信息
  ```rust
    fn main(){
        let v = vec![1,2,3];
        println!("{}",v[233]);      //panic
    }
  ```
  `RUST_BACKTRACE=1 cargo run`
### 可恢复错误 `Result<T,E>`
  ```rust
      use std::fs::File;
      use std::io::ErrorKind;
      fn open_file_print_length(path: &str) {
          let file = File::open(path);
          let file = match file {
              Ok(x) => { x }
              Err(e) => match e.kind() {
                  ErrorKind::NotFound => match File::create(path) {
                    Ok(f) => { f }
                    Err(e) => { panic!("创建文件错误{}", e) }
                  }
                  _ => { panic!("无法打开文件{}", e) }
              }
          };
          if let Ok(meta) = file.metadata() {
              println!("file length is {}", meta.len());
          }
      }
  ```
- *closure* 
  ```rust
      fn open_file_print_length_v2(path:&str){
          let file = File::open(path).unwrap_or_else(|error|{
              if error.kind() == ErrorKind::NotFound{
                  File::create(path).unwrap_or_else(|error|{
                      panic!("创建文件错误{}",error)
                  })
              }else { 
                  panic!("无法打开文件{}",error)
              }
          });
          if let Ok(meta) = file.metadata(){
              println!("file length is {}",meta.len());
          }
      }
  ```
- Result::unwrap()
  返回Ok的值，否则panic
- Result::expect(msg: &str)
  返回Ok的值，接收panic要提示的字符串
  
- 错误传递
  ```rust
    fn read_file_to_string(path:&str)-> Result<String,Error>{
        return match File::open(path) {
            Ok(mut f) => {
                let mut s = String::new();
                return match f.read_to_string(&mut s){
                    Ok(_) => {Ok(s)}
                    Err(e) => {Err(e)}
                }
            }
            Err(e) => { Err(e) }
        }
    }
  ```
- `?`操作符--更简洁的错误传递
  ```rust
      fn read_file_to_string_v2(path:&str) ->Result<String,Error> {
          let mut file = File::open(path)?;
          let mut content = String::new();
          file.read_to_string(&mut content)?;
          Ok(content)
      }
  ```
  - `?`操作符也可用于`Option<T>`    
    当值为*None*则返回None
    ```rust
        fn last_char_of_first_line(text: &str)->Option<char>{
            text.lines().next()?.chars().last()
        }
    ```
- main函数返回其他值`Result<(),Box<dyn Error>>`
```rust
fn main() ->Result<(),Box<dyn Error>>{
    let f = File::open("hello.txt")?;
    let x = f.metadata().unwrap().len();
    println!("{x}");
    Ok(())
}
```
### 创建自定义类型来进行验证
- 猜数游戏中并没有校验用户输入在1-100间，而每次用户输入后检验显得冗长
  
*guess.rs*
```rust
pub struct Guess{
    value:i32    //value对外不可见，避免用户跳过校验直接生成Guess
}

impl Guess {
    pub fn new(value:i32)->Guess{
        if value<1 || value>100 {
            panic!("Guess num should be less than 100 and greater than 1")
        }
        Guess{value}
    }
    pub fn value(&self)->i32{
        self.value
    }
}
```
*main.rs*
```rust
use guess::Guess;
fn main() {
    //--snip--
    loop{
        //--snip--
        std::io::stdin().read_line(&mut input).expect("读取输入错误");
        let guess:Guess = match input.trim().parse(){
            Ok(x) => Guess::new(x),
            Err(_) => {println!("请输入1-100数字");continue}
        };
        let v = guess.value();
        //--snip--
    }
}
```

## 泛型、特性、生命周期
### 泛型
#### 函数泛型
```rust
fn main() {
    println!("============================================================================================");

    let v = vec![1,2,3,4,5,5];
    let largest_in_v = largest(&v);
    print!("largest is {largest_in_v}")
}
fn largest<T: std::cmp::PartialOrd>(v: &[T])->&T{
    let mut x = &v[0];
    for i in v{
        if x<i{
            x = i;
        }
    }
    x
}
```
#### 结构泛型
```rust
struct Point<T> {
    x:T,y:T,
}

impl<T> Point<T> {
    fn x(&self)->&T{
        &self.x
    }
}

impl Point<f32> {
  //只f32类型的Point能使用该函数
  fn distance_to(&self,other:&Self)->f32{
    ((self.x - other.x).powi(2)+(self.y-other.y).powi(2)).sqrt()
  }
}
```
### 特性
类似与接口(*interface*)的概念
```rust
pub trait SayHi{
  fn hi(&self)->String;
  //带有默认实现
  fn hello(&self)->String { 
    "hello"
  }
}
pub struct Cat{
  name:String
}
impl SayHi for Cat {
  fn hi(&self) -> String {
    format!("{}: {}",self.name,"meow~")
  }

  fn hello(&self) -> String {
    format!("{}: {}",self.name,"hello, meow~")
  }
}
```
- 要为一个类型T实现特性S，**T与S至少有一个必须在当前crate中才能实现**
#### 特性作为参数
  ```rust
    pub fn greeting(x:&impl SayHi){
        println!("x is greeting: {}",x.hi());
    }
    fn main() {
        let c = Cat{name:"Tom".to_string()};
        greeting(&c);
    }
  ```
- 语法糖
  ```rust
    pub fn greeting2(x:&impl SayHi,y:&impl SayHi){//--snip--//
    }
  ```
  多个相同特性出现可以简写,但是简写后两个参数类型就必须一致
  ```rust
    pub fn greeting22<T:SayHi>(x:&T,y:&T){
    }
  ```
- 多个特性使用`+`间隔
  ```rust
    pub fn greeting_farewell(x:&(impl Sayhi + SayGoodbye)){}
  ```
- `where`
  ```rust
    fn some_function<T,U>(t:&T,u:&U)->i32 
        where T:Display+Clone,U:Clone+Debug{
        0
    }
  ```
#### 返回值
  ```rust
    fn create_say_hi(s:String,cat_or_dog:bool) ->impl SayHi{
        if(cat_or_dog){
            Cat{name:s}
        }else {
            Dog{name:s}    //编译错误！！！！if-else返回类型不一致
        }
    }
  ```
#### 泛型+特性
  ```rust
    struct Pair<T> {a:T,b:T}
    impl<T: SayHi> Pair<T> {
        fn say_hi_together(&self)->String{
            format!("{}\n{}",self.a.hi(),self.b.hi())
        }
    }
  ```

### 生命周期
- 悬挂引用
  ```rust
    fn main() {
        let r;
        {
            let a = 5;
            r = &a;
        }       //变量a的声明周期至此结束
        println!("r is {}",r);
    }
  ```
- 泛型在函数中的声明周期
  ```rust
  fn bigger<'a,T: std::cmp::PartialOrd>(a:&'a T,b:&'a T)->&'a T{
      if a>b {
          a
      }else {
          b
      }
  }
  ```
- 生命周期注解只是告知编译器将参数与返回值的周期联系起来
  ```rust
  fn main() {
      let sa = String::from("a string");
      let c;
      {
          let sb = String::from("another string");
          c = longer(sa.as_str(), sb.as_str());
      }
      println!("longer is {c}");   //编译错误 sb.as_str()的值生命周期结束了
  }
  
  fn longer<'a>(a: &'a str, b: &'a str) -> &'a str {
      if a.len() > b.len() {
          a
      } else {
          b
      }
  }
  ```
- 结构体上的生命周期注解
  ```rust
  fn main() {
      println!("============================================================================================");
      let s = String::from("this is a string. just a string");
      let first_sentence = s.split('.').next().expect("msg");
      let x = HoldStrRef{part:first_sentence};
      print!("{}",x.part);
  }
  struct HoldStrRef<'a>{
      part: &'a str,
  }
  ```
- 静态生命周期: 软件运行期间都有效
  ```rust
  fn main(){
      let s: &'static str = "this is a static lifetime.";
  }
  ```
#### lifetime elision rules
- 给每一个入参一个单独的生命周期
- 如果入参只有一个，那么它就和所有返回值共享一个生命周期
- 如果入参之中有一个`&self` 或者`&mut self`, 那么所有返回值和self标记一个生命周期

## 测试
### 单元测试
```rust
#[cfg(test)] 
mod tests{
    use std::fs::File;

    #[test]
    fn test1(){
        print!("默认不会打印展示");
        assert_eq!(2+2,4);
    }
    #[test]
    #[should_panic(expected = "蓄意的错误")]
    fn test2(){
        assert_ne!(2 + 2, 4, "这是蓄意的错误：{}", "2+2!=4");
    }
    #[test]
    fn failed_example(){
        panic!("this will fail");
    }
    #[test]
    fn result_test(){
        let result = File::open("not_exist_file");
        assert!(result.is_err())
    }
    #[test]
    #[ignore]   //默认情况下测试跳过
    fn some_test_take_long_time(){
        //code that takes long time
    }
    #[test]
    fn test_subset_1(){
    }
    #[test]
    fn test_subset_2(){
    }
    #[test]
    fn test_subset_3(){
    }
}
```
- 执行`cargo test`,默认并发执行所有测试并捕获所有输出以阻止其打印
  - `cargo test -- --test-threads=1`
  - `cargo test -- --show-output` print宏命令打印的内容会被展示
  - `cargo test -- --ignored`  仅执行被ignore的测试例
  - `cargo test -- --include-ignored`  被ignore的测试例一起执行
  - `cargo test failed_example` 仅执行某个测试例
  - `cargo test subset` 仅执行带有'subset'的测试例
- `#[cfg(test)]` 告知编译器**仅在**执行cargo test时编译其中内容
- 通过`use xxx::*` 测试例允许直接测试其他模块中未被声明为`pub`的函数
### 集成测试
即测试lib crate整体，只能使用暴露的api，相当于将lib create作为外部引入的crate进行测试

在src并列位置建立tests文件夹，其中添加文件*integration_test.rs*
*tests/integration_test.rs*
  ```rust
  use city;
  #[test]
  fn test1(){
      let i = city::add(1, 1);
      assert!(i,2);
  }
  ```
- tests文件夹中创建子模块建议使用tests/mod_name/mod.rs的方式，
  使用test/mod_name.rs的方式会导致测试输出中包含mod_name的相关输出，即便mod_name中没有测试用例也没有被调用
- 如果项目中只有*src/main.rs*而没有*src/lib.rs*，则无法使用use引入main中定义的函数进行测试，
  即，只有lib crate中的函数才能被集成测试的用例作为外部crates使用

## 写一个命令行工具--grep
- cargo 传递参数
`cargo run -- searchingstring example.txt`

*src/main.rs*
  ```rust
    use std::{env, process};
    use minigrep::GrepArgs;
    fn main() {
        let args: Vec<String> = env::args().collect();  //args函数应付不了无效unicode字符会panic
        let grep_args = GrepArgs::build(args).unwrap_or_else(|err|{
            panic!("{err}")
        });
        
        println!("search string '{}' in file: {}", grep_args.search_text(), grep_args.file_path());
        if let Err(e) = minigrep::run_grep(grep_args){
            println!("Error: {}",e);
            process::exit(1);
        };  
    }
  ```
*src/lib.rs*
  ```rust
  use std::{fs, error::Error};
  pub struct GrepArgs{
      file_path:String, search_text:String,
  }
  impl GrepArgs {
      pub fn build(args:Vec<String>)->Result<GrepArgs,&'static str> {
          if args.len()<3 {
              Err("not enough params")
          }else {
              Ok(GrepArgs{
                  file_path:args[1].clone(),
                  search_text: args[2].clone()
              })
          }
      }
      pub fn search_text(&self)->&String{
          &self.search_text
      }
      pub fn file_path(&self)->&String{
          &self.file_path
      }
  }
  pub fn run_grep(args:GrepArgs)->Result<(),Box<dyn Error>>{
      let content = fs::read_to_string(args.file_path)?;
      println!("{content}");
      Ok(())
  }
  ```
## 迭代器与闭包 
### 闭包
```rust
fn closure_example(){
    let add_one_v1 = |x:u32|-> u32{ x+1 };
    let add_one_v1 = |x|          { x+1 };
    let add_one_v1 = |x|            x+1 ;
}
```
- 类型推断，一个闭包只有一种类型
  ```rust
  fn main(){
      let a_closure = |x|x;
      let s = a_closure(String::from("string"));
      let n = a_closure(4);               //编译错误, a_closure的参数被推断为String
  }
  ```
#### 闭包所有权
```rust
fn main() {
    let mut v = vec![1,2,3];
    let mut push_a_v = || v.push(4);  //mutable borrow
    print!("{:?}",v);           //immutable borrow 编译错误
    push_a_v();
    print!("{:?}",v);
}
```

- Fn 特性    
  闭包会自动实现以下几种特性种的一种或多种
  - `FnOnce` 至少被调用一次，可能不能调用多次
  - `FnMut` 不会转移捕获值所有权, 但是可能更改
    > don't move captured values out of their body
  - `Fn` 可能更改也可能转移
  > 下例中sort_by_key接收参数为FnMut，但push方法处转移了所有权
  ```rust
    fn main() {
        let mut v = vec![Point{x:1,y:2},Point{x:3,y:2},Point{x:4,y:2}];
        let s = String::from("count key a time");
        let mut count_time:Vec<String> = Vec::new();
        v.sort_by_key(|v|{
            count_time.push(s);  //编译错误 
            v.x
        });
    }
    struct Point{
        x:i32,y:i32
    }
  ```
- 如果没有capturing value的需求，应该使用函数名而非闭包，如
  对于一个`Option<Vec<T>>`，可以使用`unwarp_or_else(Vec::new)`

### 迭代器
  ```rust
  pub trait Iterator { 
      type Item;
      fn next(&mut self) -> Option<Self::Item>;
  }
  ```
- 三种迭代器
```rust
  fn main() {
      let mut v = vec![Point { x: 1, y: 2 }, Point { x: 3, y: 2 }, Point { x: 4, y: 2 }];
      let it1 = v.iter();       //immutable ref
      let it2 = v.iter_mut();   //mutable ref
      let it3 = v.into_iter();  //take ownership of v, return owned value
  }
```
- 迭代器方法
  - *consuming adaptor* 使用该类方法会用掉迭代器，即拿走迭代器所有权，如`sum`
    ```rust
      fn it_method(){
          let v = vec![1,2,4,5,3];
          let it = v.iter();
          let total:i32 = it.sum();
          //it 失效
          println!("{total}");
      }
    ```
  - *iterator adaptor* 不会用掉迭代器，返回一个新的迭代器表示结果，如map     
    只是调用map方法的话不会进行计算，因为
    > iterators are lazy and do nothing unless consumed
    ```rust
      fn it_method(){
          let v = vec![1,2,4,5,3];
          let x = v.iter().map(|x|x+1);  
          let v2:Vec<_> = x.collect();  //调用consuming adaptor method
          println!("{:?}",v2)
      }
    ```
  - 获取所有权
    ```rust
    fn main() {
        let v = vec![Point{x:1,y:2},Point{x:1,y:1},Point{x:3,y:2},Point{x:1,y:3}];
        let v2 = special_point(v);
        print!("{:?}",v2)
        
    }
    #[derive(Debug)]
    struct Point{
        x:i32,y:i32
    }
    fn special_point(v:Vec<Point>)->Vec<Point>{
        v.into_iter().filter(|x|x.x==x.y).collect()
    }
    ```
- 循环or迭代器
  ```rust
  fn main() {
      let mut buffer = [2,3,4,0,89,2,34,6,2];
      let coeffiences = [1,2,3];
      let qlp_shift: i16 = 3;
      for i in 3..buffer.len(){
          println!("i{i}");
          let p = coeffiences.iter().zip(&buffer[i-3..i]).map(|(&c,&s)|c*s as i64).sum::<i64>() >> qlp_shift;
          buffer[i] += p  ;
      }
      println!("{:?}",buffer)
  }
  ```

## cargo
### 使用profile自定义构建
`cargo build --[release|dev]`
以下是没有显式写出来的默认配置
Cargo.toml
```toml
[profile.dev]
opt-level=0
[profile.release]
opt-level=3
```
### 在crates.io上发布代码
- 文档注释使用`///`支持markdown语法
```rust
/// add one
/// # Example
/// ```
/// let x = 2;
/// let y = add_one(x);
/// assert_eq!(x,3);
/// ```
fn add_one(x:i32) -> i32{
  x+1
}
```
- 使用`cargo doc`可以生成html文档，`cargo doc --open`可以直接在浏览器中打开代码中编写的文档以及依赖的crate的文档
- `# Example`是常用的文档注释块之一，其他的还有
  - Panic  Errors  Safety
- `cargo test`会执行example中的代码作为测试
- `//!`型注释用以描述crate的意图，包含哪些函数api等;常在lib.rs或mod.rs中使用
```rust
//! # My Create
//! this is a toolset for something...
//! ...
```
- 使用`pub use`可以让外部用户便于寻找关键api    
  例如： 用户需要一个类型 `my_create::mod1::mod1x::CustomTypeX;` 
      只需在src/lib.rs中声明 `pub use self::mod1::mod1x::CustomTypeX`
      用户在外部就可直接使用`user my_create::CustomTypeX`来引入该类型
- 一些元信息
```toml
[package]
name = "my_create"
version = "0.0.1"
editon = "2022"
license = "GPL"
```
- 注册账户 得到api key 使用`cargo login [apikey]`进行登录
- 使用 `cargo publish`发布该crate
- 旧的版本不能删除，但是可是使用 `cargo yank --vers 0.0.1`来阻止被其他新的项目依赖
-  `cargo yank --vers 0.0.1 --undo`可以对该操作进行撤销

### cargo 工作空间
*Cargo.toml*
```toml
[workspace]
members = [
    "adder",
    "add_one",
]
```
- 在此目录中执行`cargo new adder` `cargo new add_one`    
- add_one中    
  *add_one/src/lib.rs*
  ```rust
  fn add_one(x:i32)->i32{
    x+1
  }
  ```
- 在adder中添加对add_one的依赖    
  *adder/Cargo.toml*
  ```toml
  [dependencies]
  add_one = {path = "../add_one"}
  ```
  使用
  ```rust
  use add_one;
  fn main(){
      let x = 1;
      println!("{}",add_one::add_one(x));
  }
  ```
- 在父级目录执行`cargo run -p adder`指定执行adder中的代码
> 注意： 工作空间只在顶层有一个Cargo.lock文件
- 同样地，`cargo test -p adder`也可指定执行某个crate

### 本地装载 `cargo install`
默认安装在*$HOME/.cargo/bin*中，若需要使用安装的二进制程序，则需要在*$PATH*中包含该路径

## 智能指针
允许多个所有者，通过跟踪所有者数量，在没有所有者时清除数据；    
`String` 和`Vec<T>` 就是之一
标准库中一些常见的smart pointer
- `Box<T>` 在堆上分配值
- `Rc<T>` 允许多所有权的引用计数类型
- `Ref<T>` 和 `RefMut<T>` 迫使borrowing value发生在运行期而非编译期
### 使用场景
  1. 某种需要的类型无法在编译时知晓其大小，该类型中的某个值需要准确的大小
  2. 当需要转移大量数据的所有权，并保证数据在过程中不会进行复制
  3. 当需要获取某个值的所有权，但不关心其具体类型，仅需要其实现某个特性
- 递归类型    
  形如`(1,(2,(3,Nil)))`的类型，rust中没有空指针的概念，试采用以下代码
  ```rust
  enum List{
      Cons(i32,List),        //编译错误，List的size无法确定
      Nil
  }
  ```
  使用`Box<List>`相当于只分配了Box所需的内存，List的大小继而确定
### Deref特性
- `Box<T>`可以像引用一样使用
  ```rust
  fn main(){
      let x = 5;
      let y = &x;
      assert_eq!(5, *y);
      let z = Box::new(x);
      assert_eq!(5, *z);
  }
  ```
- Box实现了如下特性
  ```rust
  struct MyBox<T>(T);
  impl<T> MyBox<T>{
      fn new(x:T)->MyBox<T>{
          MyBox(x)
      }
  }
  impl<T> Deref for MyBox<T>{
      type Target = T;
      fn deref(&self) -> &Self::Target{
          &self.0
      }
  }
  fn main(){
      let x = MyBox::new(5);
      assert_eq!(5,*x);
  }
  ```
  当使用`*x`时，相当于使用`*(x.deref())`
- String类型实现了Deref特性，返回`&str`

#### 函数与方法的隐式deref coercion
  ```rust
  fn hello(s:&str) {
      println!("{},{}","hello",s) 
  }
  fn main(){
      let x = MyBox::new(String::from("Rust"));
      hello(&x);      //rust 会自动进行 x -> String -> &str 的转换
      //若没有deref coercion则需要如下调用
      hello(&(*x)[..])
  }
  ```
rust会仅可能的分析变量类型并调用Deref::deref方法来匹配函数参数类型，这都发生的编译期间，故不会有性能损失
- 可变引用可以自动转换为可变引用，不可变引用不能转换为可变引用。

### Drop特性
```rust
struct MyResource{
    res: SomeResource   //需要额外操作进行资源释放的类型，如文件句柄，套接字，锁，等
}
impl Drop for MyResource{
    fn drop(&mut self) {
        self.res.close();
    }
}
```
- 变量离开作用域自动调用(显式调用会出错)
- 提前释放资源使用 `std::mem::drop`函数

### 引用计数型智能指针 Rc<T>
- 仅用于单线程
- 对于像图结构一样的数据，Box<T>类型会出错
  ```rust
  fn main() {
      let l = List::Cons(1, Box::new(List::Cons(2, Box::new(List::Nil))));
      let l1 = List::Cons(-1, Box::new(l));
      let l2 = List::Cons(-2, Box::new(l));     //编译错误，l所有权已经给了l1了，
  }
  enum List<T>{
      Cons(T,Box<List<T>>),
      Nil,
  }
  ```
- 将Box替换为Rc
  ```rust
  fn main() {
      let l = Rc::new(List::Cons(1, Rc::new(List::Cons(2, Rc::new(List::Nil)))));
      let l1 = List::Cons(-1, Rc::clone(&l));
      let l2 = List::Cons(-2, Rc::clone(&l));
  }
  enum List<T>{
      Cons(T,Rc<List<T>>),
      Nil,
  }
  ```
- 每次Rc::clone引用计数都会增加，我们也可以调用 `l.clone()`但是这样会进行深层拷贝，而Rc::clone只会增加引用计数
- 只能用于不可变引用：使用可变引用必然造成同一变量的多个可变引用而发生编译错误

### 可变引用RefCell<T>
- 即使RefCell<T>是不可变的，仍然可以改变其中的值
- RefCell<T>不会在编译期检查borrowing规则的完备，而在运行时进行检查，若违背规则会panic
    ```rust
    pub trait StatusMsg {
        fn process(&self,msg:&str);
    }
    
    pub struct MyData<'a, T:StatusMsg>{
        status_repoter:&'a T,
        data: i32,
    }
    impl<'a,T> MyData<'a,T> where T:StatusMsg{
        fn new(msg_reporter:&'a T)->MyData<'a, T>{
            MyData { status_repoter: msg_reporter, data: 0 }
        }
        fn set_data(&mut self,x:i32){
            self.status_repoter.process("设置数据");
            self.data = x;
        }
    }
    ```
    为测试以上代码
    ```rust
    #[cfg(test)]
    mod test{
        use std::cell::RefCell;
    
        use crate::{StatusMsg, MyData};
    
        #[derive(Debug)]
        struct MyStatusReport{
            msg_vec:RefCell<Vec<String>>
        }
        impl MyStatusReport{
            fn new()->MyStatusReport{
                MyStatusReport { msg_vec: RefCell::new(vec![]) }
            }
        }
        impl StatusMsg for MyStatusReport {
            fn process(&self,msg:&str) {    
                //要保存消息必然要修改MyStatusReport，而特性使用的是不可变引用，故此处使用RefCell在运行时borrow一个可变引用
                self.msg_vec.borrow_mut().push(String::from(msg));    
            }
        }
        #[test]
        fn test1(){
            let reporter = MyStatusReport::new();
            let mut x = MyData::new(&reporter);
            x.set_data(2);
            assert_eq!(reporter.msg_vec.borrow().len(),1);
        }
    }
    ```
- 运行时错误示例
    ```rust
    #[test]
    fn test2(){
        let x = MyStatusReport::new();
        let mut y = x.msg_vec.borrow_mut();
        let mut z = x.msg_vec.borrow_mut();     //panicked at 'already borrowed: BorrowMutError'
        y.push(String::from("1"));
        z.push(String::from("2"));
    }
    ```
### 多所有者可变引用Rc<RefCell<T>>
```rust
#[derive(Debug)]
struct List<T>{
    data: RefCell<T>,
    next:Option<Rc<List<T>>>,
}

#[cfg(test)]
mod test{
  use std::{rc::Rc, borrow::BorrowMut, cell::RefCell, ops::Deref};
  use crate::List;
  #[test]
  fn test1(){
    let x = Rc::new(
      List{data:RefCell::new(1),
        next:Some(Rc::new(List{data:RefCell::new(2),next:None}))});
    let x1 = List{data:RefCell::new(-1),next:Some(Rc::clone(&x))};
    let x2 = List{data:RefCell::new(-1),next:Some(Rc::clone(&x))};  //x的多个引用
    *x1.data.borrow_mut() = -2;
    *x2.data.borrow_mut() = -3;         //运行时检查borrow
    if let Some(t) = &x1.next{
      println!("{}",t.data.borrow());
      *t.data.borrow_mut() = 111;
    };
    println!("{:?}",x1);
    println!("{:?}",x2);
    println!("{:?}",x);
  }
}
```

### Weak<T> 弱引用，避免内存泄漏
```rust
struct Node{
    data:i32, parent:RefCell<Weak<Node>>, children:RefCell<Vec<Rc<Node>>>
}
```

## 并发
```rust
fn main() {
    let v = vec![1,2,3];
    let t1 =  thread::spawn(||{
        println!("{:?}",v);
        for i in v{
            thread::sleep(Duration::from_secs(i))
        }
    });
    // println!("{:?}",v); //编译错误
    t1.join().unwrap();  //等待线程结束，返回Result
}
```
### 线程间数据传递
  ```rust
  fn main() {
      let (tx,rx) = mpsc::channel();
      let tx2 = tx.clone();          //多个发送方
      let t1 = thread::spawn(move||{              //move关键字指示闭包捕获数据所有权
          let v = vec![String::from("hello"),String::from("rust"),String::from("do")];
          for i in v{
              tx.send(i).unwrap();
              thread::sleep(Duration::from_secs(1));
          }
      });
      let v = vec![2,3,1];
      let t2 = thread::spawn(move||{
          for i in v{
              tx2.send(i.to_string()).unwrap();
              thread::sleep(Duration::from_secs(1));
          }
      });
      let t = thread::spawn(move||{
          for i in rx{                //迭代器使用rx的recv方法阻塞当前线程(try_recv方法立即返回)
              println!("{i}");
          }
      });
      t.join().unwrap();
  }
  ```
### 互斥量 Mutex<T>
```rust
fn main() {
    let mutex_x = Mutex::new(0);            //mutex会被move到闭包中
    let share_cout = Arc::new(mutex_x);     //故使用智能指针，Rc<T>仅支持单线程
    let mut v = Vec::new();
    for i in 0..10{
        let count = share_cout.clone();
        let t = thread::spawn(move||{
            let mut x = count.lock().unwrap();
            *x += 1;
        });
        v.push(t);
    }
    for i in v{
        i.join().unwrap();
    }
    let x = share_cout.lock().unwrap();
    println!("{:?}",x);
}
```
- Rc<T>的多线程版本Arc<T> （atomically reference count）
- Arc<Mutex<T>> 与Rc<RefCell<T>> 类似，后者会出现内存泄漏，前者与之对应的会产生死锁

### 特性 Sync与Send
- Send允许在线程间传递所有权，绝大部分类型都是实现了Send特性的，出去一些例外，如Rc<T>
- 任何实现了Send特性的不可变引用类型，都是Sync的，即允许被多个线程使用
- 手动实现这两种特性是*unsafe*的

## 面向对象特征

- 封装：隐藏实现细节，通过pub关键字可以做到实现细节的隐藏
- 继承：不使用macro，rust无法继承父结构中的字段与方法实现
- 多态：子类型当作父类型来用，rust用泛型generic和特性trait来做到这一点
> 子类不应总是共享父类的所有特性，但是继承会这么做。这减少了程序设计的灵活性

```rust

trait animal{
    fn hello(&self);
}
struct Zoo{
    members: Vec<Box<dyn animal>>
}
impl Zoo {
    fn run(&self){
        for i in self.members.iter(){
            i.hello()
        }
    }
}
struct Cat{}
impl animal for Cat {
    fn hello(&self) {
        println!("mewo~~")
    }
}
struct Dog{}
impl animal for Dog {
    fn hello(&self) {
        println!("wang wang")
    }
}
fn main() {
    let z = Zoo{members:vec![
        Box::new(Cat{}),Box::new(Dog{})
    ]};
    z.run();
}
```

## 高级特性

### Unsafe
自行保证代码内存的安全，使用`unsafe`关键字，在其新的`{}`块中可以进行以下操作
- deref原生指针
- 调用unsafe的方法
- access 或修改一个静态可变变量
- 实现unsafe特性
- access `union`类型
#### 原生指针
- 允许同时多个可变或不可变的指针，指向同一位置
- 不保证指向有效内存
- 允许为空
- 不会自行释放内存
```rust
fn main() {
    //指向某个位置
    let address = 0xaabbcusize;
    let r = address as *const i32;
    //指向同一个位置
    let mut x = 5;
    let px1 = &x as *const i32;
    let px2 = &mut x as *mut i32;
    //对原生指针的deref仅在unsafe中被允许
    unsafe{
        println!("px1 is{}",*px1);
        println!("px2 is{}",*px2);
        *px2 = 3;
    }
    println!("x is {x},now");
}
```
#### 封装unsafe的方法
```rust
use std::slice;
fn main() { 
    let mut v = vec![1,2,3,4,5];
    let v  = &mut v[..];
    let (v1,v2) = split_at(v, 3);
    println!("{:?}",v1);
    println!("{:?}",v2);
}
fn split_at(v: &mut[i32],mid:usize)->(&mut[i32],&mut[i32]){
    let length = v.len();
    assert!(mid<length);
    let ptr = v.as_mut_ptr();
    unsafe{
        (
            slice::from_raw_parts_mut(ptr, mid),
            slice::from_raw_parts_mut(ptr.add(mid), length-mid)
        )
    }
}
```
#### 使用`extren`调用其他语言的方法
Foreign Function Interface
```rust
extern "C"{
    fn abs(input:i32)->i32;
}
fn main(){
    unsafe {
        println!("absolute value of -3 is {}",abs(-3));
    }
}
```
- 其中`"C"`定义了外部函数要使用的ABI(application binary interface)
- 其他语言调用rust
  ```rust
  #[no_mangle]
  pub extern "C" fn call_from_c(){
      println!("you are calling me from C code");
  }
  ```
  - 其中注解`#[no_mangle]`告知rust编译器不要进行mangle
  > mangle是指编译器会给函数添加额外编译的信息而改变函数本身的签名
#### 访问或修改静态可变变量
```rust
static mut MY_COUNT:i32 = 0;

fn add_count(){
    unsafe{
        MY_COUNT += 1;
    }
}
fn main(){
    add_count();
    unsafe{
        println!("my count is {} now ",MY_COUNT);
    }
}
```
#### 实现unsafe特性
```rust
unsafe trait Foo{
    fn some_unsafe_method();
}
unsafe impl Foo for i32{
    //...
}
```
只要特性中至少有一个方法是unsafe的，那么该特性就是unsafe的

### trait的高级特性
####类型占位符
```rust
struct X{}
pub trait SomeTrait {
    type Item;
    fn get_a(&self)->Option<Self::Item>;
}
impl SomeTrait for X {
    type Item=i32;
    fn get_a(&self)->Option<Self::Item> {
        Some(23333)
    }
}
fn main(){
    let x = X{};
    let a = x.get_a();
    println!("{:?}",a);
}
```
- 类型占位符，使得特性被实现后不必额外提供注解来指示,且实现一次。典型例子`Iterator`
```
pub trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;
}
```
- 换言之如果特性是泛化的，则代表它可以被实现多次，如下所示
```rust
trait SomeTrait<T>{
    fn get_a(&self)->Option<T>;
}
impl SomeTrait<i32> for X {
    fn get_a(&self)->Option<i32> {
        Some(12345)
    }
}
impl SomeTrait<String> for X {      //多次实现泛化特性
    fn get_a(&self)->Option<String> {
        Some("12345".to_string())
    }
}
fn main(){
    let x = X{};
    let a:Option<i32> = x.get_a();      //类型注解指示要调用哪个实现
    println!("{:?}",a);
}
```
#### 默认泛化类型参数 与 操作符重载
- `+`号操作符特性如下，其中`<Rhs = Self>`即代表Rhs的默认类型
```rust
pub trait Add<Rhs = Self> {
    type Output;
    fn add(self, rhs: Rhs) -> Self::Output;
}
```
为类型实现操作符重载
```rust
use std::ops::Add;
#[derive(Debug)]
struct Meter(i32);
impl Add for Meter{
    type Output = Meter;
    fn add(self, rhs: Self) -> Self::Output {
        Meter(self.0+rhs.0)
    }
}
fn main() {
    let x = Meter(2);
    let y = Meter(3);
    let z = x+y;
    println!("{:?}",z);
}
```
- 可以实现不同类型的相加操作
```rust
#[derive(Debug)]
struct KiloMeter(i32);
impl Add<KiloMeter> for Meter {
    type Output = Meter;
    fn add(self, rhs:KiloMeter)->Self::Output{
        Meter(self.0+rhs.0*1000)
    }
}
fn main() {
    let a = Meter(2);
    let b = KiloMeter(1);
    let c = a+b;
    println!("{:?}",c);
}
```

#### 方法同名
```rust
struct Dog{}
trait Pilot{fn fly(&self);}
trait Wizard{fn fly(&self);}
impl Dog {
    fn fly(&self) {
        println!("wang jump")
    }
}
impl Pilot for Dog {
    fn fly(&self) {
        println!("this is captain speaking")
    }
}
impl Wizard for Dog{
    fn fly(&self){
        println!("Up!")
    }
}
trait Animal{fn baby_name()->String;}       //没有self参数的特性方法
impl Animal for Dog {
    fn baby_name()->String {                //重名方法
        String::from("puppy")
    }
}
impl Dog{
    fn baby_name()->String{
        String::from("spot")
    }
}
fn main(){
    let d = Dog{};
    d.fly();
    Pilot::fly(&d);     //多个同名方法，特性名来确定调用哪一个
    Wizard::fly(&d);
    println!("Its name is {}",Dog::baby_name());
    //没有self参数的，使用全限定名调用方式
    println!("a baby dog is called a {}",<Dog as Animal>::baby_name());     //特性方法调用
}
```
#### 父特性
要实现的特性M依赖于特性B；即要为类型T实现特性M时，T必须已实现了特性B；
则称B为M的*Supertrait*
```rust
use std::fmt;
trait OutlinePrint: fmt::Display{       //依赖父特性
    fn outline_print(&self){
        let output = self.to_string();
        let len = output.len();
        println!("{}","*".repeat(len+4));
        println!("*{}*"," ".repeat(len+2));
        println!("* {} *",output);
        println!("*{}*"," ".repeat(len+2));
        println!("{}","*".repeat(len+4));
    }
}
struct Point(i32,i32);
impl fmt::Display for Point{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,"({},{})",self.0,self.1)
    }
}
impl OutlinePrint for Point{}
fn main(){
    let p = Point(1,2);
    p.outline_print();
}
```

### 类型的高级特性

#### 类型别名
- 长名字变简洁
```rust
type Thunk<T> = Box<dyn Fn(T) +Send+'static> ;
fn fn_take_a_long_name_type(f:Thunk<i32>){
    let y = 23333;
    f(y);
}
fn main(){
    let say_hi:Thunk<i32> = Box::new(|x|{println!("hi, {x}")});
    fn_take_a_long_name_type(say_hi);
}
```

#### 不返回类型 `Never Type`
```rust
fn bar()->!{
    loop{
        println!("never return");
    }
}
```
- `loop`表达式即是一个典型的 never type表达式
- 类似的`continue`,`panic!`
```rust
fn main(){
    loop{
    //--snip--
        let guess:u32 = match guess.trim.parse() {
            Ok(num) => num,
            _ => continue,
        };
    //--snip--
    }
}
```
#### 动态大小的类型
```rust
fn generic<T:?Sized>(t:&T){
}
```
- `?Sized`表明T的可能是也可能不是`Sized`的

### 函数与闭包的高级特性

#### 函数指针
`fn` 关键字是一种函数指针类型，区分于特性`FN`，函数可以作为函数指针类型的参数传递；    
该类型实现了所有的闭包特性(即`Fn+FnMut+FnOnce`)
```rust
fn main(){
    enum Status {
        Value(u32),
        Stop,
    }
    let v:Vec<Status> = (0u32..23).map(Status::Value).collect();
}

```
#### 闭包作为返回值
```rust
fn get_a_closure()->Box<dyn Fn(i32)->i32>{
    Box::new(|x:i32|{x+1})
}
fn main(){
    let func1 = get_a_closure();
    let x = func1(2);
    println!("{x}")
}
```
### 宏
- 三种宏
  - `#[derive]`在结构体和枚举类型上的
  - attribute-like
  - function-like
#### 函数与宏
宏可以接收可变数量的参数`println!("{x}")`, `println!("{}",x)` 
宏可以给指定类型实现某个特性
#### 元编程
```rust
#[macro_export]
macro_rules! vec_simple {
    ($($x:expr),*) => {
        {
            let mut tmp_v = Vec::new();
            $(
                tmp_v.push($x);
            )*
            tmp_v
        }
    };
}
```
- `#[macro_export]` 使得其他的crate也是可以看到这个宏
- 为每个匹配到`$()`中的内容，执行`$()*`中的内容，

#### derive宏
derive只能作用于struct与enum
- 建立两个crate
  ```shell
  cargo new my_macro --lib
  cargo new my_macro_derive --lib
  ```
- *my_macro_derive/Cargo.toml*
  ```toml
  [package]
  name = "my_macro_derive"
  version = "0.1.0"
  edition = "2021"
  
  [lib]
  proc-macro = true
  
  [dependencies]
  syn = "1.0"       #将rust代码解析为可操作的数据
  quote = "1.0"     #将syn数据转化为rust代码
  ```
- *my_macro/lib.rs*
  ```rust
  pub trait MyDerive {
      fn print_struct_name();
  }
  ```
- *my_macro_derive/lib.rs*
  ```rust
  use proc_macro::TokenStream;
  use syn;
  use quote::quote;
  #[proc_macro_derive(MyDerive)]
  pub fn my_derive(input:TokenStream)->TokenStream{
      let a_syntax_tree = syn::parse(input).unwrap();
      impl_my_derive(&a_syntax_tree)
  }
  fn impl_my_derive(a_syntax_tree: &syn::DeriveInput) -> TokenStream {
      let name = &a_syntax_tree.ident;
      let gen = quote!{
          impl MyDerive for #name{
              fn print_struct_name(){
                  println!("this is code generated by macro==={}",stringify!(#name));
              }
          }
      };
      gen.into()
  }
  ```
- 使用    
  - crate中引入两个crate
  ```toml
  [dependencies]
  my_macro = {path = "../my_macro"}
  my_macro_derive = {path = "../my_macro_derive"}
  ```
  ```rust
  use my_macro::MyDerive;
  use my_macro_derive::MyDerive;
  #[derive(MyDerive)]
  struct TestMacro{}
  fn main() {
      TestMacro::print_struct_name();
  }
  ```
#### 其他宏 attribute-like function-like
- attribute-like 例
  ```rust
  #[route(GET,"/")]
  fn index(){}
  ```
  ```rust
  #[proc_macro_attribute]
  pub fn route(attr:TokenStream, item:TokenStream)->TokenStream{todo!()}
  ```
  与derive工作方式类似的，`attr`表示`GET,"/"`部分； `item`表示`fn index(){}`部分
- function-like 例
  ```rust
  fn some_fn(){
        let sql = sql!(select * from table_x where id =1);
  }
  ```
  ```rust
  #[proc_macro]
  pub fn sql(input:TokenStream)->TokenStream{todo!()}
  ```
  与`macro_rules!`相似，但是`marco_rules`仅使用match匹配就可以完成定义


## web server
- http request response
  ```
  Method Request-URI HTTP-Version CRLF
  headers CRLF
  message-body
  ```
  ```
  HTTP-Version Status-Code Reason-Phrase CRLF
  headers CRLF
  message-body
  ```
- lock的声明周期与表达式
  `if let` `while let` `match`等表达式在结束前不会drop掉临时变量，
  导致`receiver.lock()`的锁会一直保持，
  ```rust
  fn some(){
      let thread = thread::spawn(move || {
          while let Ok(job) = receiver.lock().unwrap().recv() {
              println!("Worker {id} got a job; executing.");
              job();    //job方法执行中并没有释放锁
          }
          //receiver锁释放
      });
  }
  fn some2(){
      let thread = thread::spawn(move ||loop{
          let job = receiver.lock().unwrap().recv().unwrap();
          //receiver锁释放
          println!("worker{id} receiver a job");
          job();
      });
  }
  ```
















